FROM node:10.15.2-alpine as base
WORKDIR /app
ENV PATH /app/node_modules/.bin:$PATH
COPY . ./
RUN npm ci --silent; \
    npm install react-scripts@3.4.1 -g --silent; \
    npm run build

# production environment
FROM nginx:stable-alpine as prod
COPY --from=base /app/build /usr/share/nginx/html
EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]